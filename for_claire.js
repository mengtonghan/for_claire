define(['pipAPI', 'https://bitbucket.org/mengtonghan/for_claire/raw/ab1eb69bae06f265631bda615b2c9f2edcc58320/logic.js'], function(APIConstructor, epExtension){
	var API = new APIConstructor();
	var global = API.getGlobal();
	return epExtension(
	{
			//The prime categories.
			primeCats :  [
				{
					name : 'Black people', //Will appear in the data.
					//An array of all media objects for this category.
					mediaArray : [
    				    {word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}
				    ]
				}, 
				{
					name : 'White people', //Will appear in the data.
					//An array of all media objects for this category.
					mediaArray : [
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}, 
    					{word : 'Love'}
    				]
				}
			],	
			
			
			nBlocks : 2,
			
			instructions : {
				//Block 1
				firstBlock : '<div><p style="font-size:1.3em; text-align:left; margin-left:10px; font-family:arial"><color="000000"><br/>' +
				'In the following trials you will see two words connected by a hyphen and your task is to determine how associated the two words are.<br/><br/>' +
				'Please indicate you response using the keyboard <b>1</b> to <b>7</b>, with 1 indicating the two words are </color> <font color="#ff0000"><b>NOT</b></font> associated at all and 7 indicating the two words are </color> <font color="#ff0000"><b>extremely</b></font> associated. ' +
				'A string of numbers and letters will act as a <b>SIGNAL</b> that the words are soon to appear.' +
				'Both signal and the words will appear right in the centre of the screen.' +
				'</color></p><p style="font-size:14px; text-align:center; font-family:arial"><color="000000"><br/><br/>' +
				'press SPACE to begin</p><p style="font-size:12px; text-align:center; font-family:arial">' +
				'<color="000000">[Round 1 of nBlocks]</p></div>',
				//Block 2
				middleBlock : '<div><p style="font-size:1.3em; text-align:left; margin-left:10px; font-family:arial"><color="000000"><br/>' +
				'Practice is over and the actual task is about to start.' +
				'The actual task is just like the practice: you will see a signal that is followed by two words connected with a hyphen.<br/><br/>' +
				'Indicate you response using the keyboard <b>1</b> to <b>7</b>, with <b>1</b> indicating the two words are </color> <font color="#ff0000"><b>NOT</b></font> associated at all and 7 indicating the two words are </color> <font color="#ff0000"><b>extremely</b></font> associated.' +
				'<p style="font-size:12px; text-align:center; font-family:arial">' +
				'<color="000000">[Round blockNum of nBlocks]</p></div>',
				//Block 3
				lastBlock : '<div><p style="font-size:1.3em; text-align:left; margin-left:10px; font-family:arial"><color="000000"><br/>' +
				'Practice is over and the actual task is about to start.' +
				'The actual task is just like the practice: you will see a signal that is followed by two words connected with a hyphen.<br/><br/>' +
				'Indicate you response using the keyboard <b>1</b> to <b>7</b>, with <b>1</b> indicating the two words are </color> <font color="#ff0000"><b>NOT</b></font> associated at all and 7 indicating the two words are </color> <font color="#ff0000"><b>extremely</b></font> associated.' +
				'<p style="font-size:12px; text-align:center; font-family:arial">' +
				'<color="000000">[Round blockNum of nBlocks]</p></div>'
			},

			nTrialsPerPrimeTargetPair:5, //How many trials in a block, per prime-target combination (always three blocks).
			fixationDuration : 500, 
			errorFBDuration : 0, 
			ITIDuration : 0,
			
			

			//Set the image folder here.
			base_url : {
				image : "https://baranan.github.io/minno-tasks/images/"
			}
	});
});